package com.grasernetwork.game;

import com.grasernetwork.core.Core;
import com.grasernetwork.game.components.game.GameManager;

public class GamePlugin extends Core
{

	@Override
	public void enable()
	{
		GameManager manager = new GameManager(this);
		manager.initialize(GameManager.Gamemode.TEST_GAME);
	}
}
