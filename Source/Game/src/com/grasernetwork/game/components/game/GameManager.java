package com.grasernetwork.game.components.game;

import com.grasernetwork.game.GamePlugin;
import com.grasernetwork.game.games.TestGame;

/**
 * Created by Teddeh on 10/03/2016.
 */
public class GameManager
{
	private static GamePlugin plugin;
	private Gamemode gamemode;

	public GameManager(GamePlugin plugin)
	{
		this.plugin = plugin;
	}

	public void initialize(Gamemode gamemode)
	{
		this.gamemode = gamemode;
		Game game = gamemode.getGameInstance();
		game.initialize();

		System.out.println("GAME INITIALIZED");
		System.out.println("GameType: " + game.getGameType());
		System.out.println("Game: " + game.getName());
		System.out.println("Id: " + game.getId());
	}

	public enum Gamemode
	{
		TEST_GAME(new TestGame(plugin));

		private Game game;

		Gamemode(Game game)
		{
			this.game = game;
		}

		public Game getGameInstance()
		{
			return game;
		}
	}
}
