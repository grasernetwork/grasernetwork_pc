package com.grasernetwork.game.components.game;

import com.grasernetwork.game.GamePlugin;

/**
 * Created by Teddeh on 08/03/2016.
 */
public abstract class Game extends GameComponent
{
	private final int id;
	private final String name;
	private final GameType gameType;

	/**
	 * This constructor should be used for all NON-ARCADE based gamemodes.
	 *
	 * @param id   The game id. -- Should never be changed! --
	 * @param name The game name.
	 */
	public Game(GamePlugin plugin, int id, String name)
	{
		super(plugin);

		this.id = id;
		this.name = name;
		this.gameType = GameType.STANDALONE;
	}

	/**
	 * This constructor should be used for games which are not Standalone games.
	 *
	 * @param id       The game id. -- Should never be changed! --
	 * @param name     The game name.
	 * @param gameType The Gametype of the game.
	 */
	public Game(GamePlugin plugin, int id, String name, GameType gameType)
	{
		super(plugin);

		this.id = id;
		this.name = name;
		this.gameType = gameType;
	}

	public abstract void initialize();

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public GameType getGameType()
	{
		return gameType;
	}
}
