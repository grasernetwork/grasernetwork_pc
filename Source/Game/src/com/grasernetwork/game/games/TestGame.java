package com.grasernetwork.game.games;

import com.grasernetwork.game.GamePlugin;
import com.grasernetwork.game.components.game.Game;
import com.grasernetwork.game.components.game.GameType;

/**
 * Created by Teddeh on 08/03/2016.
 */
public class TestGame extends Game
{
	public TestGame(GamePlugin plugin)
	{
		super(plugin, 1, "Test Game", GameType.STANDALONE);
	}

	@Override
	public void initialize()
	{

	}
}
